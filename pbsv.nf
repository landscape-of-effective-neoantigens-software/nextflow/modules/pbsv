#!/usr/bin/env nextflow

process pbsv_discover  {
// Runs pbsv discover
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq1) - FASTQ 2
//   parstr - Additional Parameters
//
// output:
//   tuple => emit: sams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.sam') - Output SAM File

// require:
//   FQS
//   params.pbsv$pbsv_mem_parameters

  tag "${dataset}/${pat_name}/${run}"
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/pbsv"
  label 'pbsv_container'
  label 'pbsv'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(bam), path(bai)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.svsig.gz'), emit: svsigs

  script:
  """
  pbsv discover \
    ${parstr} \
    -s ${dataset}-${pat_name}-${run} \
    ${bam} \
    ${dataset}-${pat_name}-${run}.svsig.gz
  """
}

process pbsv_call  {
// Runs pbsv call
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq1) - FASTQ 2
//   parstr - Additional Parameters
//
// output:
//   tuple => emit: sams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.sam') - Output SAM File

// require:
//   FQS
//   params.pbsv$pbsv_mem_parameters

  tag "${dataset}/${pat_name}/${run}"
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/pbsv"
  label 'pbsv_container'
  label 'pbsv'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(svsig)
  path fa
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.vcf'), emit: vcfs

  script:
  """
  pbsv call \
    ${parstr} \
    ${fa} \
    ${svsig} \
    ${dataset}-${pat_name}-${run}.pbsv.vcf
  """
}
